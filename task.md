## Task 1

Add the delete method to delete key-value pairs of the HashTable table, which is implemented in the synopsis.

## Task 2

Implement a binary search for a sorted array with fractional numbers. The written function for binary search must return a tuple, where the first element is the number of iterations required to find the element. The second element should be the "upper bound" - the smallest element that is greater than or equal to the given value.

## Task 3

Compare the efficiency of the substring search algorithms: Boyer-Moore, Knuth-Morris-Pratt, and Rabin-Karp based on two text files (article 1, article 2). Using timeit, measure the execution time of each algorithm for two types of substrings: one that exists in the text and the other that is made up (the choice of substrings is up to you). Based on the data obtained, determine the fastest algorithm for each text individually and as a whole.

## Task 1:

The code is executed, and the delete method deletes the specified key-value pair in the HashTable table.

## Task 2:

The code returns a tuple, where the first element is the number of iterations required to find the element. The second element is the "upper bound" (the smallest element that is greater than or equal to the specified value).

## Task 3:

Programmatically implement the algorithms for finding a substring: Boyer-Moore, Knuth-Morris-Pratt, and Rabin-Karp.

Based on the implementation of each of the three algorithms, the fastest algorithm for each text is determined.

Conclusions are drawn about the speed of the algorithms for each text separately and in general. The conclusions are presented in the form of a markdown document.

Grading format
Pass/Fail