def binary_search(arr, x):
    low = 0
    high = len(arr) - 1
    iterations = 0
    upper_bound = None

    while low <= high:
        mid = (high + low) // 2
        iterations += 1

        # If x is greater, ignore left half
        if arr[mid] < x:
            low = mid + 1

        # If x is smaller, ignore right half
        elif arr[mid] > x:
            high = mid - 1
            upper_bound = arr[mid]

        # x is present at mid
        else:
            return iterations, arr[mid]

    # If we reach here, then the element was not present
    return iterations, upper_bound

# Define a sorted array with fractional numbers
arr = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

# Define a value to search for
x = 0.85

# Call the binary search function
result = binary_search(arr, x)

# Print the result
print(f"Number of iterations: {result[0]}, Upper bound: {result[1]}")