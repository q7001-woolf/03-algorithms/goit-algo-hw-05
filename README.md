
```
Execution time of kmp_search for substring "static" in article: 0.00235179999981483
Execution time of kmp_search for substring "algorithm" in article: 0.00010160000056202989
Execution time of kmp_search for substring "static" in article: 0.004526599999735481
Execution time of kmp_search for substring "algorithm" in article: 4.6999999540275894e-05
Execution time of boyer_moore_search for substring "static" in article: 0.0010700999991968274
Execution time of boyer_moore_search for substring "algorithm" in article: 3.6299999919719994e-05
Execution time of boyer_moore_search for substring "static" in article: 0.0025451000001339708
Execution time of boyer_moore_search for substring "algorithm" in article: 2.0899999071843922e-05
Execution time of rabin_karp_search for substring "static" in article: 0.005273499999020714
Execution time of rabin_karp_search for substring "algorithm" in article: 0.0002815000007103663
Execution time of rabin_karp_search for substring "static" in article: 0.01202709999961371
Execution time of rabin_karp_search for substring "algorithm" in article: 0.00015560000065306667
```

## Knuth-Morris-Pratt (KMP)
The execution time for finding 'static' ranged from approximately 0.00235 to 0.00453 seconds, and for 'algorithm' it ranged from approximately 0.00005 to 0.00010 seconds.

## Boyer-Moore
The execution time for finding 'static' ranged from approximately 0.00107 to 0.00255 seconds, and for 'algorithm' it ranged from approximately 0.00002 to 0.00004 seconds.

## Rabin-Karp
The execution time for finding 'static' ranged from approximately 0.00527 to 0.01203 seconds, and for 'algorithm' it ranged from approximately 0.00016 to 0.00028 seconds.

## Summary
From these results, we can observe that the Boyer-Moore algorithm had the shortest execution time for both substrings in both articles, making it the most efficient among the three in this scenario. The Rabin-Karp algorithm had the longest execution time, making it the least efficient.